<?php

namespace App\Http\Controllers;

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class FormatriceController extends Controller
{
    public function index(){
        try {
            $categories = DB::table('formatrice')->get();
            return view('views.index', ['formatrice' => $categories]);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    public function index1(){
        try {
            $categories = DB::table('formatrice')->get();
            return $categories;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    public function delete($id)
    {
        try {
            DB::table('formatrice')->where('idc', '=', $id)->delete();
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    public function put( Request $request, $id)
    {
        try {
            DB::table('formatrice')->where('Idc', '=', $id)->update([
                'label' => $request->label
            ]);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    public function post(Request $request)
    {
       try {
        DB::table('formatrice')->insert([
            'nom' => $request->nom,
            'prenom'=>$request->prenom,
            'adresse'=>$request->adresse,
            'tel'=>$request->tel,
            'email'=>$request->email,
            'Pseudo'=>$request->Pseudo,
            'IDT'=>$request->IDT
        ]);
       } catch (\Throwable $th) {
           //throw $th;
       }
    }
    public function filter(Request $req)
    {
        try{
            $user = DB::table('formatrice')->where([
                ['Pseudo','=',$req->Pseudo],
                ['password','=',$req->password]
            ])->get();
            return [
                "etat"=>1,
                "data"=>$user,
            ];
           
        }
       catch(\Throwable $th)
       {
            return $th->getMessage();
       }
    }
    public function GetFormations(Request $request)
    {
       try{
        $formation = DB::table('formation')->get();
        return $formation;
       }
       catch(\Throwable $th)
       {
            return $th->getMessage();
       }
    }
    public function AddFormation(Request $request)
    {
       try {
        DB::table('formation')->insert([
            'Libellé' => $request->Libellé,
            'DateDebut'=>$request->DateDebut,
            'DateFin'=>$request->DateFin,
            'Id'=>$request->Id,
            'IDT'=>$request->IDT,
        ]);
       } catch (\Throwable $th) {
           //throw $th;
       }
    }
    public function GetTypeFormation()
    {
        try{
            $type = DB::table('typeformation')->get();
            return $type;
        }
        catch(\Throwable $th)
        {
            return $th->getMessage();
        }
    }
    public function GetCandidat()
    {
        try{
            $candidat = DB::table('candidat')->get();
            return $candidat;
           }
           catch(\Throwable $th)
           {
                return $th->getMessage();
           }
    }
}

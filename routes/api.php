<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FormatriceController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/t',[FormatriceController::class,'index1']);
Route::delete('/{id}',[FormatriceController::class,'delete']);
Route::put('/{id}',[FormatriceController::class,'put']);
Route::post('/',[FormatriceController::class,'post']);
Route::post('/t/t',[FormatriceController::class,'filter']);
Route::get('/t/a',[FormatriceController::class,'GetFormations']);
Route::post('/t/b',[FormatriceController::class,'AddFormation']);
Route::get('/t/c',[FormatriceController::class,'GetTypeFormation']);
Route::get('/t/d',[FormatriceController::class,'GetCandidat']);